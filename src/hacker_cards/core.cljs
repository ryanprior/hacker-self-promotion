(ns hacker-cards.core
  (:require [cljs-http.client :as http]
            [cljs.core.async :refer [>! <! go chan]]
            [goog.dom]
            [om.next :as om :refer-macros [defui]]
            [om.dom :as dom]))

(enable-console-print!)

(defn text->deck [text]
  (->> (clojure.string/split text "\n")
       (map (partial re-find #"\*\*.*"))
       (filter identity)
       (map #(subs % 2))
       (map clojure.string/trim)))

(def hacker-self-promotion-deck-url "/self-promotion-deck.org")
(defonce app-state (atom {:cards []
                          :current-hand []
                          :num-cards 4}))

(defn update-num-cards! [event]
  (let [new-value (js/parseInt event.target.value)]
    (swap! app-state assoc :num-cards new-value)))

(defn fetch-deck-from-url [deck-url]
  (let [channel (chan)]
    (go (let [response (<! (http/get deck-url {:use-credentials :false}))]
          (>! channel (-> response :body text->deck))))
    channel))

(defn deal! [num-cards deck]
  (swap! app-state assoc :current-hand (->> deck shuffle (take num-cards))))

(defui CardDrawer
  Object
  (render [this]
          (let [num-cards (:num-cards (om/props this))
                update-function (:update (om/props this))
                draw-function (:draw (om/props this))]
            (dom/div #js {:className "card-drawer"}
                     (dom/form #js {:onSubmit draw-function}
                               "New hand of "
                               (dom/input #js {:type "number" :value num-cards :onChange update-function})
                               " cards: "
                               (dom/button #js {:onClick draw-function} "Draw"))))))
(def card-drawer (om/factory CardDrawer))

(defui Hand
  Object
  (render [this]
          (dom/ul #js {:className "hand"}
                  (->> (om/children this) (map-indexed #(dom/li #js {:key %1} %2))))))
(def hand (om/factory Hand))

(defui Card
  Object
  (render [this]
          (let [card-text (:text (om/props this))]
            (dom/div #js {:className "card"}
                     card-text))))
(def card (om/factory Card))

(defui HackerCardsApp
  Object
  (render [this]
          (let [deck      (:cards @app-state)
                cards     (:current-hand @app-state)
                num-cards (:num-cards @app-state)]
            (dom/div nil
                     (card-drawer {:num-cards num-cards
                                   :update update-num-cards!
                                   :draw (fn [event]
                                           (.preventDefault event)
                                           (deal! num-cards deck))})
                     (apply hand nil
                            (map-indexed #(card {:react-key %1 :text %2}) cards))))))

;; (def app (om/factory HackerCardsApp))

(def reconciler (om/reconciler {:state app-state}))

;; (js/ReactDOM.render (app {:header (:text @app-state)}) (goog.dom/getElement "app"))
(om/add-root! reconciler HackerCardsApp (goog.dom/getElement "app"))

(defn init! []
  (go
    (let [deck (<! (fetch-deck-from-url hacker-self-promotion-deck-url))]
      (swap! app-state assoc :cards deck))))

(js/window.addEventListener "DOMContentLoaded" init!)

(defn on-js-reload [])
