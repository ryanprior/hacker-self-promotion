(ns hacker-cards.cli)
;; (ns hacker-cards.cli
;;   (:require [clojure.string :as string]
;;             [fs :as fs]
;;             [cljs.core.async :as async :refer [<! >! chan go]]
;;             [hacker-cards.cards :as cards :refer [parse-org cards]]))

;; (defn read-deck [file-name channel]
;;   (.readFile fs file-name
;;              {:encoding "utf-8"}
;;              (fn [err data]
;;                (go (if err
;;                      (throw err)
;;                      (->> data parse-org (>! channel)))))))

;; (defn -main []
;;   (let [channel (chan 2)]
;;     (read-deck "self-promotion-deck.org" channel)
;;     (go (->> (<! channel) cards shuffle (take 3) (string/join "\n") (.log js/console)))))
