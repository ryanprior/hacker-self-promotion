(ns hacker-cards.cards)

(defn header-text [node]
  (-> node .-children first .-children first .-value))

(defn nodes-of-level [level nodes]
  (filter (fn [node] (= node.level level))
               nodes))

;; (defn read-deck [file-name channel]
;;   (.readFile fs file-name
;;              {:encoding "utf-8"}
;;              (fn [err data]
;;                (go (if err
;;                      (throw err)
;;                      (->> data str (.parse (parser)) (>! channel)))))))

;; (def _parser (atom nil))
;; (defn parser [] (or @_parser
;;                     (swap! _parser (constantly (org.Parser.)))))
;; (defn parse-org [text]
;;   (->> text str (.parse (parser))))
(defn cards [text]
  (->> (clojure.string/split "\n" text)
       (map (partial re-find #"(?<=^\*\*).*$"))
       (filter identity)
       (map (clojure.string/trim))))

;; (defn cards [deck]
;;   (->> deck.nodes
;;        (nodes-of-level 2)
;;        (map header-text)))

(defn text->deck [text]
  (-> text cards))

;; (defn -main []
;;   (let [channel (chan 2)]
;;     (read-deck "self-promotion-deck.org" channel)
;;     (go (->> (<! channel) cards shuffle (take 3) (string/join "\n") log))))
